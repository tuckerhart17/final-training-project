
class UniqueUri < ActiveModel::Validator
  def validate(record)
    @event = Event.find_by_uri(record.uri)
    if @event != nil
      record.errors.add(:uri, "taken already")
    end
  end
end