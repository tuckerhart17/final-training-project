require 'date'
module EventsHelper
  def time_until(start_t, end_t)
    now = DateTime.now
    if now < start_t and day_gap(start_t, end_t) > 0
      days = day_gap(start_t, end_t)
      return "Starts in #{days} #{"day".pluralize(days)}"
    elsif now < start_t
      return "Starts at #{start_t.to_time}"
    elsif now < end_t
      return "Ends at #{end_t.to_time}"
    else
      return "Event Over"
    end   
  end

  def day_gap(start_t, end_t)
    (start_t.to_date - end_t.to_date).to_int
  end
end
