#module EventHelpers
#  public
  def fill_fields()
    fill_in "Name", :with => "Birthday Bash"
    fill_in "Location", :with => "Nepal"
    select_date("2020,December,7", :from => "Start")
    select_time("02", "00", :from => "Start")
    select_date("2020,December,7", :from => "End")
    select_time("03", "00", :from => "End")
    fill_in "Creator", :with => "Tucker Hart"
    fill_in "Contact email", :with => "tucker.hart@example.com"
    fill_in "Uri", :with => "TestUri"
  end
#end
